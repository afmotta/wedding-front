import React from 'react';
import { Switch } from 'react-router';
import { renderRoutes } from 'react-router-config';

import App from 'components/App';
import {
  HomePage,
  GiftListPage,
  GiftDetailPage,
  AcquirePage,
} from 'components';

export const routes = [
  {
    path: '/',
    exact: true,
    component: HomePage,
  },
  {
    path: '/gifts',
    exact: true,
    component: GiftListPage,
  },
  {
    path: '/gifts/:id',
    component: GiftDetailPage,
  },
  {
    path: '/acquire',
    component: AcquirePage,
  },
];

const AppRoutes = () => (
  <App>
    <Switch>
      {renderRoutes(routes)}
    </Switch>
  </App>
);

export default AppRoutes;
