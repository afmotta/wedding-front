import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import { getSchema } from '../api/sagas';

export function* readCategoryList(api, params) {
  const schema = yield call(getSchema);
  try {
    const data = yield call([api, api.action], schema, ['categories', 'list'], { ...params });
    yield put(actions.categoryListReadSuccess(data));
  } catch (e) {
    yield put(actions.categoryListReadFailure(e));
  }
}

export function* watchCategoryListReadRequest(api) {
  while (true) {
    const { params } = yield take(actions.CATEGORY_LIST_READ_REQUEST);
    yield call(readCategoryList, api, params);
  }
}

export default function* ({ api }) {
  yield fork(watchCategoryListReadRequest, api);
}
