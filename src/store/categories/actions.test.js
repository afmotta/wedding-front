import * as actions from './actions';

test('categoryListReadRequest', () => {
  expect(actions.categoryListReadRequest({ fields: 'test' })).toEqual({
    type: actions.CATEGORY_LIST_READ_REQUEST,
    params: { fields: 'test' },
  });
});

test('categoryListReadSuccess', () => {
  expect(actions.categoryListReadSuccess([1, 2, 3])).toEqual({
    type: actions.CATEGORY_LIST_READ_SUCCESS,
    list: [1, 2, 3],
  });
});

test('categoryListReadFailure', () => {
  expect(actions.categoryListReadFailure('error')).toEqual({
    type: actions.CATEGORY_LIST_READ_FAILURE,
    error: 'error',
  });
});
