import { initialState } from './selectors';
import { CATEGORY_LIST_READ_SUCCESS } from './actions';

export default (state = initialState, action) => {
  switch (action.type) {
    case CATEGORY_LIST_READ_SUCCESS:
      return {
        ...state,
        list: action.list,
      };
    default:
      return state;
  }
};
