import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import saga, * as sagas from './sagas';
import { getSchema } from '../api/sagas';

const api = {
  action: () => {},
};

const schema = {};

describe('readCategoryList', () => {
  it('calls success', () => {
    const data = [1, 2, 3];
    const generator = sagas.readCategoryList(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['categories', 'list'], {}));
    expect(generator.next(data).value)
      .toEqual(put(actions.categoryListReadSuccess(data)));
  });

  it('calls failure', () => {
    const generator = sagas.readCategoryList(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['categories', 'list'], {}));
    expect(generator.throw('test').value)
      .toEqual(put(actions.categoryListReadFailure('test')));
  });
});

test('watchCategoryListReadRequest', () => {
  const payload = { params: { _limit: 1 } };
  const generator = sagas.watchCategoryListReadRequest(api);
  expect(generator.next().value)
    .toEqual(take(actions.CATEGORY_LIST_READ_REQUEST));
  expect(generator.next(payload).value)
    .toEqual(call(sagas.readCategoryList, api, payload.params));
});

test('saga', () => {
  const generator = saga({ api });
  expect(generator.next().value)
    .toEqual(fork(sagas.watchCategoryListReadRequest, api));
});
