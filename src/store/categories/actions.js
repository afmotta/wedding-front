export const CATEGORY_LIST_READ = 'CATEGORY_LIST_READ';
export const CATEGORY_LIST_READ_REQUEST = 'CATEGORY_LIST_READ_REQUEST';
export const CATEGORY_LIST_READ_SUCCESS = 'CATEGORY_LIST_READ_SUCCESS';
export const CATEGORY_LIST_READ_FAILURE = 'CATEGORY_LIST_READ_FAILURE';

export const categoryListReadRequest = (params, resolve, reject) => ({
  type: CATEGORY_LIST_READ_REQUEST,
  params,
  resolve,
  reject,
});

export const categoryListReadSuccess = list => ({
  type: CATEGORY_LIST_READ_SUCCESS,
  list,
});

export const categoryListReadFailure = error => ({
  type: CATEGORY_LIST_READ_FAILURE,
  error,
});
