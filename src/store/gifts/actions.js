export const GIFT_LIST_READ = 'GIFT_LIST_READ';
export const GIFT_LIST_READ_REQUEST = 'GIFT_LIST_READ_REQUEST';
export const GIFT_LIST_READ_SUCCESS = 'GIFT_LIST_READ_SUCCESS';
export const GIFT_LIST_READ_FAILURE = 'GIFT_LIST_READ_FAILURE';

export const giftListReadRequest = (params, resolve, reject) => ({
  type: GIFT_LIST_READ_REQUEST,
  params,
  resolve,
  reject,
});

export const giftListReadSuccess = list => ({
  type: GIFT_LIST_READ_SUCCESS,
  list,
});

export const giftListReadFailure = error => ({
  type: GIFT_LIST_READ_FAILURE,
  error,
});

export const GIFT_DETAIL_READ = 'GIFT_DETAIL_READ';
export const GIFT_DETAIL_READ_REQUEST = 'GIFT_DETAIL_READ_REQUEST';
export const GIFT_DETAIL_READ_SUCCESS = 'GIFT_DETAIL_READ_SUCCESS';
export const GIFT_DETAIL_READ_FAILURE = 'GIFT_DETAIL_READ_FAILURE';

export const giftDetailReadRequest = (params, resolve, reject) => ({
  type: GIFT_DETAIL_READ_REQUEST,
  params,
  resolve,
  reject,
});

export const giftDetailReadSuccess = detail => ({
  type: GIFT_DETAIL_READ_SUCCESS,
  detail,
});

export const giftDetailReadFailure = error => ({
  type: GIFT_DETAIL_READ_FAILURE,
  error,
});
