import * as actions from './actions';

test('giftListReadRequest', () => {
  expect(actions.giftListReadRequest({ fields: 'test' })).toEqual({
    type: actions.GIFT_LIST_READ_REQUEST,
    params: { fields: 'test' },
  });
});

test('giftListReadSuccess', () => {
  expect(actions.giftListReadSuccess([1, 2, 3])).toEqual({
    type: actions.GIFT_LIST_READ_SUCCESS,
    list: [1, 2, 3],
  });
});

test('giftListReadFailure', () => {
  expect(actions.giftListReadFailure('error')).toEqual({
    type: actions.GIFT_LIST_READ_FAILURE,
    error: 'error',
  });
});
