import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import { getSchema } from '../api/sagas';

export function* readGiftList(api, params) {
  const schema = yield call(getSchema);
  try {
    const data = yield call([api, api.action], schema, ['gifts', 'list'], { wedding_id: 1, ...params });
    yield put(actions.giftListReadSuccess(data));
  } catch (e) {
    yield put(actions.giftListReadFailure(e));
  }
}

export function* watchGiftListReadRequest(api) {
  while (true) {
    const { params } = yield take(actions.GIFT_LIST_READ_REQUEST);
    yield call(readGiftList, api, params);
  }
}

export function* readGiftDetail(api, params) {
  const schema = yield call(getSchema);
  try {
    const data = yield call(
      [api, api.action], schema, ['gifts', 'read'],
      { wedding_id: 1, id: params.id, ...params });
    yield put(actions.giftDetailReadSuccess(data));
  } catch (e) {
    yield put(actions.giftDetailReadFailure(e));
  }
}

export function* watchGiftDetailReadRequest(api) {
  while (true) {
    const { params } = yield take(actions.GIFT_DETAIL_READ_REQUEST);
    yield call(readGiftDetail, api, params);
  }
}

export default function* ({ api }) {
  yield fork(watchGiftListReadRequest, api);
  yield fork(watchGiftDetailReadRequest, api);
}
