import { initialState } from './selectors';
import { GIFT_LIST_READ_SUCCESS, GIFT_DETAIL_READ_SUCCESS } from './actions';

export default (state = initialState, action) => {
  switch (action.type) {
    case GIFT_LIST_READ_SUCCESS:
      return {
        ...state,
        list: action.list,
      };
    case GIFT_DETAIL_READ_SUCCESS:
      return {
        ...state,
        detail: action.detail,
      };
    default:
      return state;
  }
};
