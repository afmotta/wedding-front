import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import saga, * as sagas from './sagas';
import { getSchema } from '../api/sagas';


const api = {
  action: () => {},
};

const schema = {};

describe('acquire', () => {
  it('calls success', () => {
    const data = { parts: 1 };
    const generator = sagas.acquire(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['gifts', 'acquire'], { wedding_id: 1 }));
    expect(generator.next(data).value)
      .toEqual(put(actions.acquireSuccess(data)));
  });

  it('calls failure', () => {
    const generator = sagas.acquire(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['gifts', 'acquire'], { wedding_id: 1 }));
    expect(generator.throw('test').value)
      .toEqual(put(actions.acquireFailure('test')));
  });
});

test('watchAcquireRequest', () => {
  const payload = { params: { wedding_id: 1 } };
  const generator = sagas.watchAcquireRequest(api);
  expect(generator.next().value)
    .toEqual(take(actions.ACQUIRE_REQUEST));
  expect(generator.next(payload).value)
    .toEqual(call(sagas.acquire, api, payload.params));
});

test('saga', () => {
  const generator = saga({ api });
  expect(generator.next().value)
    .toEqual(fork(sagas.watchAcquireRequest, api));
});
