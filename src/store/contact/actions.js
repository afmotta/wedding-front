export const CONTACT = 'CONTACT';
export const CONTACT_REQUEST = 'CONTACT_REQUEST';
export const CONTACT_SUCCESS = 'CONTACT_SUCCESS';
export const CONTACT_FAILURE = 'CONTACT_FAILURE';

export const contactRequest = (params, resolve, reject) => ({
  type: CONTACT_REQUEST,
  params,
  resolve,
  reject,
});

export const acquireSuccess = result => ({
  type: CONTACT_SUCCESS,
  ...result,
});

export const acquireFailure = error => ({
  type: CONTACT_FAILURE,
  error,
});
