import { take, put, call, fork, select } from 'redux-saga/effects';
import fetchJsonp from 'fetch-jsonp';
import * as actions from './actions';

const url = 'https://api.instagram.com/v1/tags/giulyalbitop/media/recent?access_token=35529428.e029fea.854135eb31ad4da1967e783ddaf2ffa4&count=9';

const instagramFetch = (nextId) => {
  const _url = nextId ? `${url}&max_tag_id=${nextId}` : url;
  return fetchJsonp(_url).then(response => response.json())
    .then(json => {
      const images = json.data.map(({ images, link }) => {
        return {
          image: images.standard_resolution.url,
          link,
        };
      });
      const nextId = json.pagination.next_max_id || '';
      const hasNext = !!nextId;
      return { images, nextId, hasNext };
    });
};

export function* readFeed() {
  try {
    const { feed } = yield select();
    const { images, hasNext, nextId } = yield instagramFetch(feed.nextId);
    yield put(actions.feedReadSuccess(images, hasNext, nextId));
  } catch (e) {
    yield put(actions.feedReadFailure(e));
  }
}

export function* watchFeedReadRequest() {
  while (true) {
    yield take(actions.FEED_READ_REQUEST);
    yield call(readFeed);
  }
}

export default function* () {
  yield fork(watchFeedReadRequest);
}

