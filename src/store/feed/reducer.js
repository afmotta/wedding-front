import { initialState } from './selectors';
import { FEED_READ_SUCCESS } from './actions';

export default (state = initialState, action) => {
  switch (action.type) {
    case FEED_READ_SUCCESS:
      return {
        ...state,
        list: [...state.list, ...action.list],
        hasNext: action.hasNext,
        nextId: action.nextId,
      };
    default:
      return state;
  }
};
