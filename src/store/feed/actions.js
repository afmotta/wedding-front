export const FEED_READ = 'FEED_READ';
export const FEED_READ_REQUEST = 'FEED_READ_REQUEST';
export const FEED_READ_SUCCESS = 'FEED_READ_SUCCESS';
export const FEED_READ_FAILURE = 'FEED_READ_FAILURE';

export const feedReadRequest = (params, resolve, reject) => ({
  type: FEED_READ_REQUEST,
  params,
  resolve,
  reject,
});

export const feedReadSuccess = (list, hasNext, nextId) => ({
  type: FEED_READ_SUCCESS,
  list,
  hasNext,
  nextId,
});

export const feedReadFailure = error => ({
  type: FEED_READ_FAILURE,
  error,
});
