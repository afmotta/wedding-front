import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import saga, * as sagas from './sagas';
import { getSchema } from '../api/sagas';

const api = {
  action: () => {},
};

const schema = {};

describe('readGiftList', () => {
  it('calls success', () => {
    const data = [1, 2, 3];
    const generator = sagas.readGiftList(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['gifts', 'list'], { wedding_id: 1 }));
    expect(generator.next(data).value)
      .toEqual(put(actions.giftListReadSuccess(data)));
  });

  it('calls failure', () => {
    const generator = sagas.readGiftList(api);
    expect(generator.next().value)
      .toEqual(call(getSchema));
    expect(generator.next(schema).value)
      .toEqual(call([api, api.action], schema, ['gifts', 'list'], { wedding_id: 1 }));
    expect(generator.throw('test').value)
      .toEqual(put(actions.giftListReadFailure('test')));
  });
});

test('watchGiftListReadRequest', () => {
  const payload = { params: { _limit: 1 } };
  const generator = sagas.watchGiftListReadRequest(api);
  expect(generator.next().value)
    .toEqual(take(actions.GIFT_LIST_READ_REQUEST));
  expect(generator.next(payload).value)
    .toEqual(call(sagas.readGiftList, api, payload.params));
});

test('saga', () => {
  const generator = saga({ api });
  expect(generator.next().value)
    .toEqual(fork(sagas.watchGiftListReadRequest, api));
});
