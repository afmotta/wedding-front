export const initialState = {
  list: [],
  hasNext: false,
  nextId: '',
};

export const getList = (state = initialState) => state.list || initialState.list;
export const getHasNext = (state = initialState) => state.hasNext || initialState.hasNext;
export const getNextId = (state = initialState) => state.nextId || initialState.nextId;
