import * as actions from './actions';

test('feedReadRequest', () => {
  expect(actions.feedReadRequest({ fields: 'test' })).toEqual({
    type: actions.FEED_READ_REQUEST,
    params: { fields: 'test' },
  });
});

test('feedReadSuccess', () => {
  expect(actions.feedReadSuccess([1, 2, 3])).toEqual({
    type: actions.FEED_READ_SUCCESS,
    list: [1, 2, 3],
  });
});

test('feedReadFailure', () => {
  expect(actions.feedReadFailure('error')).toEqual({
    type: actions.FEED_READ_FAILURE,
    error: 'error',
  });
});
