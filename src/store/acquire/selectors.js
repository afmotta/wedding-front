export const initialState = {
  cost: null,
  gift: null,
  parts: null,
  step: null,
};

export const getCost = (state = initialState) => state.cost || initialState.cost;
export const getGift = (state = initialState) => state.gift || initialState.gift;
export const getParts = (state = initialState) => state.parts || initialState.parts;
export const getRedirect = (state = initialState) => !!state.step || !!initialState.step;
export const getStep = (state = initialState) => state.step || initialState.step;
