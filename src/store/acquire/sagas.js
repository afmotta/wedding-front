import { take, put, call, fork } from 'redux-saga/effects';
import * as actions from './actions';
import { getSchema } from '../api/sagas';

export function* acquire(api, params) {
  const schema = yield call(getSchema);
  try {
    const data = yield call([api, api.action], schema, ['gifts', 'acquire'], { wedding_id: 1, ...params });
    yield put(actions.acquireSuccess(data));
  } catch (e) {
    yield put(actions.acquireFailure(e));
  }
}

export function* watchAcquireRequest(api) {
  while (true) {
    const { params } = yield take(actions.ACQUIRE_REQUEST);
    yield call(acquire, api, params);
  }
}

export default function* ({ api }) {
  yield fork(watchAcquireRequest, api);
}
