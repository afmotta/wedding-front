import { initialState } from './selectors';
import * as actions from './actions';
import reducer from './reducer';

const states = [
  {
    parts: 5,
    gift: { name: 'gift 0' },
    cost: null,
  }, {
    parts: 1,
    gift: { name: 'gift 1' },
    cost: null,
  },
];

it('returns the initial state', () => {
  expect(reducer(undefined, {})).toEqual(initialState);
});

describe('ACQUIRE_INIT', () => {
  it('sets parts and gift in the initial state', () => {
    expect(reducer(initialState, {
      type: actions.ACQUIRE_INIT,
      ...states[0],
    })).toEqual({
      ...initialState,
      ...states[0],
      step: 'form',
    });
  });

  it('overrides parts and gift in an existing state', () => {
    expect(reducer({
      ...states[0],
    }, {
      type: actions.ACQUIRE_INIT,
      ...states[1],
    })).toEqual({
      ...initialState,
      ...states[1],
      step: 'form',
    });
  });
});

describe('ACQUIRE_SUCCESS', () => {
  it('sets parts in the initial state', () => {
    expect(reducer(states[0], {
      type: actions.ACQUIRE_SUCCESS,
      acquiredParts: 5,
      totalCost: 500,
    })).toEqual({
      ...states[0],
      step: 'success',
      parts: 5,
      cost: 500,
    });
  });
});

describe('ACQUIRE_RESET', () => {
  it('sets parts in the existing state', () => {
    expect(reducer(states[0], {
      type: actions.ACQUIRE_RESET,
    })).toEqual({
      ...initialState,
    });
  });
});
