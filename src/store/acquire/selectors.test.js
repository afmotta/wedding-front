import * as selectors from './selectors';

const altState = {
  parts: 1,
  gift: { name: 'gift' },
  step: 'form',
  cost: 100,
};

test('initialState', () => {
  expect(selectors.initialState).toEqual({
    gift: null,
    parts: null,
    step: null,
    cost: null,
  });
});

test('getCost', () => {
  expect(selectors.getCost()).toBe(selectors.initialState.cost);
  expect(selectors.getCost(altState)).toBe(altState.cost);
});

test('getGift', () => {
  expect(selectors.getGift()).toBe(selectors.initialState.gift);
  expect(selectors.getGift(altState)).toBe(altState.gift);
});

test('getParts', () => {
  expect(selectors.getParts()).toBe(selectors.initialState.parts);
  expect(selectors.getParts(altState)).toBe(altState.parts);
});

test('getRedirect', () => {
  expect(selectors.getRedirect()).toBe(!!selectors.initialState.step);
  expect(selectors.getRedirect(altState)).toBe(!!altState.step);
});

test('getStep', () => {
  expect(selectors.getStep()).toBe(selectors.initialState.step);
  expect(selectors.getStep(altState)).toBe(altState.step);
});
