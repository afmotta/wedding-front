import { initialState } from './selectors';
import { ACQUIRE_INIT, ACQUIRE_SUCCESS, ACQUIRE_RESET } from './actions';

export default (state = initialState, action) => {
  switch (action.type) {
    case ACQUIRE_INIT:
      return {
        ...state,
        gift: action.gift,
        parts: action.parts,
        step: 'form',
      };
    case ACQUIRE_SUCCESS:
      return {
        ...state,
        parts: action.acquiredParts,
        cost: action.totalCost,
        step: 'success',
      };
    case ACQUIRE_RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
