export const ACQUIRE = 'ACQUIRE';
export const ACQUIRE_INIT = 'ACQUIRE_INIT';
export const ACQUIRE_RESET = 'ACQUIRE_RESET';
export const ACQUIRE_REQUEST = 'ACQUIRE_REQUEST';
export const ACQUIRE_SUCCESS = 'ACQUIRE_SUCCESS';
export const ACQUIRE_FAILURE = 'ACQUIRE_FAILURE';

export const acquireRequest = (params, resolve, reject) => ({
  type: ACQUIRE_REQUEST,
  params,
  resolve,
  reject,
});

export const acquireSuccess = result => ({
  type: ACQUIRE_SUCCESS,
  ...result,
});

export const acquireFailure = error => ({
  type: ACQUIRE_FAILURE,
  error,
});

export const initAcquire = (gift, parts) => ({
  type: ACQUIRE_INIT,
  gift,
  parts,
});

export const acquireReset = () => ({
  type: ACQUIRE_RESET,
});
