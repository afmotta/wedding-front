import * as actions from './actions';

test('acquireRequest', () => {
  expect(actions.acquireRequest({ fields: 'test' })).toEqual({
    type: actions.ACQUIRE_REQUEST,
    params: { fields: 'test' },
  });
});

test('acquireSuccess', () => {
  expect(actions.acquireSuccess({ cost: 500 })).toEqual({
    type: actions.ACQUIRE_SUCCESS,
    cost: 500,
  });
});

test('acquireFailure', () => {
  expect(actions.acquireFailure('error')).toEqual({
    type: actions.ACQUIRE_FAILURE,
    error: 'error',
  });
});

test('initAcquire', () => {
  expect(actions.initAcquire({ name: 'gift 1' }, 1)).toEqual({
    type: actions.ACQUIRE_INIT,
    gift: { name: 'gift 1' },
    parts: 1,
  });
});

test('acquireReset', () => {
  expect(actions.acquireReset()).toEqual({
    type: actions.ACQUIRE_RESET,
  });
});
