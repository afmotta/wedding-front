export const initialState = { schema: null };

export const getSchema = (state = initialState) => state.schema || initialState.schema;
