import { put, call, fork, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as actions from './actions';

export function* getSchema() {
  let schema;
  while (true) {
    schema = yield select(({ api }) => api.schema || null);
    if (schema) {
      return schema;
    }
    yield call(delay, 100);
  }
}

export function* readSchema(api) {
  try {
    const schema = yield call([api, api.get], api.__apiUrl);
    yield put(actions.schemaReadSuccess(schema));
  } catch (e) {
    yield put(actions.schemaReadFailure(e));
  }
}

export default function* ({ api }) {
  yield fork(readSchema, api);
}
