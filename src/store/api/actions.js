export const SCHEMA_READ_SUCCESS = 'SCHEMA_READ_SUCCESS';
export const SCHEMA_READ_FAILURE = 'SCHEMA_READ_FAILURE';

export const schemaReadSuccess = schema => ({
  type: SCHEMA_READ_SUCCESS,
  schema,
});

export const schemaReadFailure = error => ({
  type: SCHEMA_READ_FAILURE,
  error,
});
