import { initialState } from './selectors';
import * as actions from './actions';
import reducer from './reducer';

it('returns the initial state', () => {
  expect(reducer(undefined, {})).toEqual(initialState);
});

describe('SCHEMA_READ_SUCCESS', () => {
  it('sets schema in the initial state', () => {
    expect(reducer(initialState, {
      type: actions.SCHEMA_READ_SUCCESS,
      schema: { gifts: 'detail' },
    })).toEqual({
      ...initialState,
      schema: { gifts: 'detail' },
    });
  });

  it('overrides schema in an existing state', () => {
    expect(reducer({
      ...initialState,
      schema: { gifts: 'detail' },
    }, {
      type: actions.SCHEMA_READ_SUCCESS,
      schema: { gifts: 'list' },
    })).toEqual({
      ...initialState,
      schema: { gifts: 'list' },
    });
  });
});
