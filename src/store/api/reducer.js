import { initialState } from './selectors';
import { SCHEMA_READ_SUCCESS } from './actions';

export default (state = initialState, action) => {
  switch (action.type) {
    case SCHEMA_READ_SUCCESS:
      return {
        ...state,
        schema: action.schema,
      };
    default:
      return state;
  }
};
