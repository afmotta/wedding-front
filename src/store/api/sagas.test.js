import { put, call, fork } from 'redux-saga/effects';
import { apiUrl } from 'config';
import * as actions from './actions';
import saga, * as sagas from './sagas';

const api = { get: () => {}, __apiUrl: 'http://test.it' };


describe('readSchema', () => {
  it('calls success', () => {
    const schema = { gifts: 'detail' };
    const generator = sagas.readSchema(api);
    expect(generator.next().value)
      .toEqual(call([api, api.get], api.__apiUrl));
    expect(generator.next(schema).value)
      .toEqual(put(actions.schemaReadSuccess(schema)));
  });

  it('calls failure', () => {
    const generator = sagas.readSchema(api);
    expect(generator.next().value)
      .toEqual(call([api, api.get], api.__apiUrl));
    expect(generator.throw('test').value)
      .toEqual(put(actions.schemaReadFailure('test')));
  });
});

test('saga', () => {
  const generator = saga({ api });
  expect(generator.next().value)
    .toEqual(fork(sagas.readSchema, api));
});
