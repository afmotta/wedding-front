import * as actions from './actions';

test('schemaReadSuccess', () => {
  expect(actions.schemaReadSuccess({ gifts: 'detail' })).toEqual({
    type: actions.SCHEMA_READ_SUCCESS,
    schema: { gifts: 'detail' },
  });
});

test('schemaReadFailure', () => {
  expect(actions.schemaReadFailure('error')).toEqual({
    type: actions.SCHEMA_READ_FAILURE,
    error: 'error',
  });
});
