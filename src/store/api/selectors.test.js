import * as selectors from './selectors';

test('initialState', () => {
  expect(selectors.initialState).toEqual({
    schema: null,
  });
});

test('getSchema', () => {
  expect(selectors.getSchema({})).toBe(selectors.initialState.schema);
  expect(selectors.getSchema()).toBe(selectors.initialState.schema);
  expect(selectors.getSchema({ schema: 'detail' })).toBe('detail');
});
