import 'react-hot-loader/patch';
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter } from 'react-router-redux';
import configureStore from 'store/configure';
import { basename } from 'config';
import api from 'services/api';
import AppRoutes from 'routes';

const history = createHistory({ basename });

const store = configureStore({}, history, { api });
const root = document.getElementById('app');

const renderApp = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AppRoutes />
    </ConnectedRouter>
  </Provider>
);

render(renderApp(), root);

if (module.hot) {
  module.hot.accept('routes', () => {
    require('routes');
    render(renderApp(), root);
  });
}
