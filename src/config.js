const merge = require('lodash/merge');

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    isDev: process.env.NODE_ENV !== 'production',
    isBrowser: typeof window !== 'undefined',
    apiUrl: 'http://127.0.0.1:8000',
  },
  test: {},
  development: {},
  production: {
    apiUrl: 'https://api.giulyalbi.top',
  },
};

module.exports = merge(config.all, config[config.all.env]);
