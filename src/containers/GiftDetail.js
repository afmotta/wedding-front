import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fromGifts, fromStatus } from 'store/selectors';
import { giftDetailReadRequest, GIFT_DETAIL_READ } from 'store/actions';

import { GiftDetail } from 'components';

class GiftDetailContainer extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    gift: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    request: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.request();
  }

  render() {
    const { gift, loading } = this.props;
    return <GiftDetail {...{ gift, loading }} />;
  }
}

const mapStateToProps = (state) => ({
  gift: fromGifts.getDetail(state),
  loading: fromStatus.isLoading(state, GIFT_DETAIL_READ),
});

const mapDispatchToProps = (dispatch, { id }) => ({
  request: () => dispatch(giftDetailReadRequest({ id })),
});

export default connect(mapStateToProps, mapDispatchToProps)(GiftDetailContainer);
