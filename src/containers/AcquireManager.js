import React from 'react';
import { connect } from 'react-redux';
import { acquireReset } from 'store/actions';

import { fromAcquire } from 'store/selectors';
import { AcquireManager } from 'components';

const AcquireManagerContainer = (props) => {
  return <AcquireManager {...props} />;
};

const mapStateToProps = (state) => ({
  cost: fromAcquire.getCost(state),
  gift: fromAcquire.getGift(state),
  parts: fromAcquire.getParts(state),
  step: fromAcquire.getStep(state),
});

const mapDispatchToProps = (dispatch) => ({
  reset: () => dispatch(acquireReset()),
});

export default connect(
  mapStateToProps, mapDispatchToProps
)(AcquireManagerContainer);
