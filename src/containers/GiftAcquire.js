import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { initAcquire } from 'store/actions';
import { Redirect } from 'react-router';

import { fromAcquire } from 'store/selectors';
import { GiftAcquire } from 'components';

const GiftAcquireContainer = (props) => {
  if (props.redirect) {
    return <Redirect to="/acquire" push />;
  }
  return <GiftAcquire {...props} />;
};

GiftAcquireContainer.propTypes = {
  gift: PropTypes.object.isRequired,
  redirect: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  redirect: fromAcquire.getRedirect(state),
});

const mapDispatchToProps = (dispatch) => ({
  initAcquire: (gift, parts) => dispatch(initAcquire(gift, parts)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GiftAcquireContainer);
