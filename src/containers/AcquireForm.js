import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { acquireRequest } from 'store/actions';
import { createValidator, required } from 'services/validation';
import { fromAcquire } from 'store/selectors';
import { AcquireForm } from 'components';

const AcquireFormContainer = (props) => <AcquireForm {...props} />;

AcquireFormContainer.propTypes = {
  parts: PropTypes.number.isRequired,
  gift: PropTypes.object.isRequired,
};

const onSubmit = (data, dispatch) => new Promise((resolve, reject) => {
  dispatch(acquireRequest(data, resolve, reject));
});

const validate = createValidator({
  message: [required],
  giver: [required],
  parts: [required],
});

const mapStateToProps = (state) => ({
  parts: fromAcquire.getParts(state),
  gift: fromAcquire.getGift(state),
});

export default connect(mapStateToProps)(reduxForm({
  form: 'AcquireForm',
  destroyOnUnmount: true,
  onSubmit,
  validate,
})(AcquireFormContainer));
