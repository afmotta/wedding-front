import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fromFeed, fromStatus } from 'store/selectors';
import {
  feedReadRequest,
  FEED_READ,
} from 'store/actions';

import { InstagramFeed } from 'components';

class InstagramFeedContainer extends Component {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.object).isRequired,
    loading: PropTypes.bool,
    request: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.request();
  }

  render() {
    return <InstagramFeed {...this.props} />;
  }
}

const mapStateToProps = (state) => ({
  list: fromFeed.getList(state),
  hasNext: fromFeed.getHasNext(state),
  loading: fromStatus.isLoading(state, FEED_READ),
});

const mapDispatchToProps = (dispatch) => ({
  request: (params = {}) => dispatch(feedReadRequest(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InstagramFeedContainer);
