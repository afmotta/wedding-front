import React from 'react';
import { reduxForm } from 'redux-form';
import { contactRequest } from 'store/actions';
import { createValidator, required, email, maxLength } from 'services/validation';
import { ContactForm } from 'components';

const ContactFormContainer = (props) => <ContactForm {...props} />;

const onSubmit = (data, dispatch) => new Promise((resolve, reject) => {
  dispatch(contactRequest(data, resolve, reject));
});

const validate = createValidator({
  message: [required],
  name: [required, maxLength(100)],
  email: [required, email],
});

export default reduxForm({
  form: 'ContactForm',
  destroyOnUnmount: true,
  onSubmit,
  validate,
})(ContactFormContainer);
