import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fromCategories, fromGifts, fromStatus } from 'store/selectors';
import {
  giftListReadRequest,
  GIFT_LIST_READ,
  categoryListReadRequest,
  CATEGORY_LIST_READ,
} from 'store/actions';

import { GiftList } from 'components';

class GiftListContainer extends Component {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.object).isRequired,
    loading: PropTypes.bool,
    request: PropTypes.func.isRequired,
    getCategories: PropTypes.func.isRequired,
  }

  componentDidMount() {
    this.props.getCategories();
    this.props.request();
  }

  render() {
    return <GiftList {...this.props} />;
  }
}

const mapStateToProps = (state) => ({
  list: fromGifts.getList(state),
  loading: fromStatus.isLoading(state, GIFT_LIST_READ),
  categories: fromCategories.getList(state),
  loadingCategories: fromStatus.isLoading(state, CATEGORY_LIST_READ),
});

const mapDispatchToProps = (dispatch) => ({
  request: (params = {}) => dispatch(giftListReadRequest(params)),
  getCategories: () => dispatch(categoryListReadRequest()),
});

export default connect(mapStateToProps, mapDispatchToProps)(GiftListContainer);
