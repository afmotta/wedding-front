import { apiUrl } from 'config';
import { Client } from 'coreapi';

const api = new Client();
api.__apiUrl = `${apiUrl}/api/schema/`;

export default api;
