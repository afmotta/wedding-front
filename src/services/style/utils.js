import { css } from 'styled-components';
import theme from 'components/themes/default';

const { sizes } = theme;

const min = {};
Object.keys(sizes).forEach((label) => {
  const emSize = sizes[label] / 16;
  min[label] = (...args) => css`
    @media (min-width: ${emSize}em) {
      ${css(...args)}
    }
  `;
});

const max = {};
Object.keys(sizes).forEach((label) => {
  const emSize = (sizes[label] + 1) / 16;
  max[label] = (...args) => css`
    @media (max-width: ${emSize}em) {
      ${css(...args)}
    }
  `;
});

export const media = { min, max };
