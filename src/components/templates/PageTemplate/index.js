import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Header, Footer } from 'components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 3.75rem;
  min-height: 100vh;
  box-sizing: border-box;
`;

const StyledHeader = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 999;
`;

const StyledHero = styled.section``;

const StyledContent = styled.section`
  width: 100%;
  box-sizing: border-box;
`;

const StyledFooter = styled.footer`
  margin-top: auto;
`;

const PageTemplate = ({ header, hero, children, footer, ...props }) => {
  return (
    <Wrapper {...props}>
      <StyledHeader>{header}</StyledHeader>
      {hero && <StyledHero>{hero}</StyledHero>}
      <StyledContent>{children}</StyledContent>
      <StyledFooter>{footer}</StyledFooter>
    </Wrapper>
  );
};

PageTemplate.propTypes = {
  header: PropTypes.node,
  hero: PropTypes.node,
  footer: PropTypes.node,
  children: PropTypes.any.isRequired,
};

PageTemplate.defaultProps = {
  header: <Header />,
  footer: <Footer />,
};


export default PageTemplate;
