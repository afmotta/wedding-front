import React from 'react';
import { PageTemplate } from 'components';
import { AcquireManager } from 'containers';

const AcquirePage = () => {
  return (
    <PageTemplate>
      <AcquireManager />
    </PageTemplate>
  );
};

export default AcquirePage;
