import React from 'react';
import scrollToComponent from 'react-scroll-to-component';
import {
  CallToAction,
  Hero,
  Infoline,
  Location,
  PageTemplate,
  Tagline,
  TitleSection,
  Storyline,
} from 'components';
import { ContactForm, InstagramFeed } from 'containers';


const HomePage = () => {
  let Contact;

  function scrollTo() {
    scrollToComponent(Contact, { duration: 2000 });
  }

  return (
    <PageTemplate hero={<Hero onClick={scrollTo} />}>
      <Tagline />
      <TitleSection title={'Cerimonia'} palette={'white'} />
      <Location
        name={'Santuario Madonna della Rotonda'}
        address={'viale del Santuario 97'}
        city={'Pumenengo (BG)'}
        photo={'/rotonda.jpeg'}
        photoAlign={'center'}
        time={'ore 15:00'}
        url={'https://goo.gl/maps/M8QkckLBeJD2'}
      />
      <Storyline
        text={'Cenni storici sul santuario mariano "La Rotonda"'}

      />
      <TitleSection title={'Ricevimento'} palette={'white'} />
      <Location
        name={'Tenuta Serradesca'}
        address={'via Serradesca 2'}
        city={'Scanzorosciate (BG)'}
        photo={'/serradesca.jpg'}
        time={'a seguire'}
        url={'https://goo.gl/maps/XqEBvtvLETu'}
        inverted
      />
      <Infoline onClick={scrollTo} />
      <CallToAction />
      <TitleSection title={'Contattaci'} palette={'white'} />
      <ContactForm ref={c => { Contact = c; }} />
      <InstagramFeed />
    </PageTemplate>
  );
};

export default HomePage;
