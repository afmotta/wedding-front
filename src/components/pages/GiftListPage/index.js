import React from 'react';
import { PageTemplate } from 'components';
import { GiftList } from 'containers';

const GiftListPage = () => {
  return (
    <PageTemplate>
      <GiftList />
    </PageTemplate>
  );
};

export default GiftListPage;
