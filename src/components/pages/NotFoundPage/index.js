import React from 'react';

import { PageTemplate, Heading } from 'components';

const NotFoundPage = () => {
  return (
    <PageTemplate>
      <Heading>404 Not Found</Heading>
    </PageTemplate>
  );
};

export default NotFoundPage;
