import React from 'react';
import PropTypes from 'prop-types';
import { PageTemplate } from 'components';
import { GiftDetail } from 'containers';

const GiftDetailPage = ({ match }) => {
  const id = match ? Number(match.params.id) : 0;
  return (
    <PageTemplate>
      <GiftDetail id={id} />
    </PageTemplate>
  );
};

GiftDetailPage.propTypes = {
  match: PropTypes.object,
};

export default GiftDetailPage;
