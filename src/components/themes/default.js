import { reversePalette } from 'styled-theme/composer';

const theme = {};

theme.palette = {
  primary: ['#95b673', '#98c967', '#a8c38c', '#b1c999', '#bbd0a5'],
  secondary: ['#c2185b', '#e91e63', '#f06292', '#f8bbd0'],
  danger: ['#d32f2f', '#f44336', '#f8877f', '#ffcdd2'],
  alert: ['#ffa000', '#ffc107', '#ffd761', '#ffecb3'],
  success: ['#388e3c', '#4caf50', '#7cc47f', '#c8e6c9'],
  grayscale: ['#212121', '#616161', '#9e9e9e', '#bdbdbd', '#e0e0e0', '#eeeeee', '#ffffff'],
  white: ['#fff', '#fff', '#eee'],
};

theme.reversePalette = reversePalette(theme.palette);

theme.fonts = {
  primary: 'Raleway, Helvetica Neue, Helvetica, Roboto, sans-serif',
  serif: 'La Belle Aurore, cursive',
  pre: 'Consolas, Liberation Mono, Menlo, Courier, monospace',
  quote: 'Georgia, serif',
};

theme.sizes = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1600,
};

export default theme;
