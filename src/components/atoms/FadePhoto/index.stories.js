import React from 'react';
import { storiesOf } from '@storybook/react';
import FadePhoto from '.';

storiesOf('FadePhoto', module)
  .add('default', () => (
    <FadePhoto {...{ backgroundImage: '/giuly.jpg' }} />
  ));
