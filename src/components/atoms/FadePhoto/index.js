import PropTypes from 'prop-types';
import styled from 'styled-components';

const FadePhoto = styled.div`
  background-image: url(${props => props.photo});
  background-size: cover;
  background-position: ${props => props.photoAlign};
  background-repeat: no-repeat;
  filter: grayscale(100%);

  &:hover {
    filter: grayscale(0%);
    transition: ease-in-out .5s;
  }
`;

FadePhoto.propTypes = {
  photo: PropTypes.string.isRequired,
  photoAlign: PropTypes.string,
};

FadePhoto.defaultProps = {
  photoAlign: 'center',
};


export default FadePhoto;
