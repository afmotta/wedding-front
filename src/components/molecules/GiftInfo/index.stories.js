import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftInfo } from 'components';

storiesOf('GiftInfo', module)
  .add('default', () => (
    <GiftInfo name="Gift n.1" description="Ullamco et reprehenderit magna cillum ullamco consectetur et enim aliqua." />
  ));
