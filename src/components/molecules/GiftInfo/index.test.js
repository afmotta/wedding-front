import React from 'react';
import { mount, shallow } from 'enzyme';
import GiftInfo from '.';

const wrap = (props = {}) => shallow(
  <GiftInfo name="test name" description="test description" price="100" {...props} />
);

it('mounts with different combination of props', () => {
  const wrapMounted = (props = {}) => mount(<GiftInfo name="a" description="b" price="1" {...props} />);
  wrapMounted();
  wrapMounted({ loading: true });
});

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo' });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(1);
});

it('renders name', () => {
  const wrapper = wrap();
  expect(wrapper.contains('test name')).toBe(true);
});

it('renders description', () => {
  const wrapper = wrap();
  expect(wrapper.contains('test description')).toBe(true);
});

it('renders price', () => {
  const wrapper = wrap();
  expect(wrapper.contains('100')).toBe(true);
});
