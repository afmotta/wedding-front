import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { palette } from 'styled-theme';

import { Badge, Heading, Paragraph } from 'components';

const Info = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const CentralSection = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 1em 0;
  text-align: center;
`;

const Item = styled.div`
  margin: 0 1em;
`;

const Title = styled(Heading)`
  margin-bottom: 0.1em;
`;

const Sub = styled.div`
  color: ${palette('grayscale', 3)};
  font-size: 70%;
`;

const GiftInfo = ({ ...props }) => {
  function getCategories(categories = []) {
    return categories.map(cat => <Badge key={cat.id}>{cat.name}</Badge>);
  }

  function getGiftCentralPart({ availableParts, totalParts, partPrice }) {
    if (!availableParts) {
      return (<CentralSection>
        <Item>
          <Title level={5} palette={'danger'}>
            Terminato
          </Title>
        </Item>
      </CentralSection>);
    }
    return (
      <CentralSection>
        <Item>
          <Title level={5} palette={'primary'}>
            {availableParts} su {totalParts}
          </Title>
          <Sub>parti disponibili</Sub>
        </Item>
        <Item>
          <Title level={5} palette={'primary'}>
            € {partPrice}
          </Title>
          <Sub>per parte</Sub>
        </Item>
      </CentralSection>
    );
  }

  return (
    <Info {...props}>
      <Title level={4} palette={'grayscale'}>
        {props.name}
      </Title>
      <Paragraph>
        {getCategories(props.categories)}
      </Paragraph>
      <Paragraph>{props.description}</Paragraph>
      {getGiftCentralPart(props)}
    </Info>
  );
};

GiftInfo.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  description: PropTypes.string,
  availableParts: PropTypes.number,
  totalParts: PropTypes.number,
  partPrice: PropTypes.string,
  categories: PropTypes.arrayOf(PropTypes.object),
};

export default GiftInfo;
