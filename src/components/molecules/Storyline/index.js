import React from 'react';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Heading } from 'components';

const Wrapper = styled.div`
  background-color: ${palette('white', 2)};
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  padding: 20px 0;
  margin: 20px 0;
`;

const Sub = styled(Heading)`
  margin: 0;
  font-size: 1.2rem;
  & > a {
      color: ${palette('grayscale', 0)};
  }
`;

const Infoline = (props) => {
  return (
    <Wrapper {...props}>
      <Sub level={6}>
        Cenni storici sul santuario mariano "La Rotonda"
      </Sub>
      <Sub level={6}>
        <a href="https://tanogabo.com/santuario-della-madonna-della-rotonda-di-pumenengo/" target="blank">
          Leggi qui
        </a>
      </Sub>
    </Wrapper>
  );
};

export default Infoline;
