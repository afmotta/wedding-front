import React from 'react';
import { storiesOf } from '@storybook/react';
import { Storyline } from 'components';

storiesOf('Storyline', module)
  .add('default', () => (
    <Storyline>Hello</Storyline>
  ))
  .add('reverse', () => (
    <Storyline reverse>Hello</Storyline>
  ));
