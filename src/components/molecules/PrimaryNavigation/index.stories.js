import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-router';

import { PrimaryNavigation } from 'components';

storiesOf('PrimaryNavigation', module)
  .addDecorator(StoryRouter())
  .add('default', () => (
    <PrimaryNavigation />
  ))
  .add('reverse', () => (
    <PrimaryNavigation reverse />
  ));
