import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import debounce from 'lodash/debounce';
import { Button, Input } from 'components';

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Search = styled(Input)`
  margin-bottom: 1em;
`;

const Categories = styled.div`
  flex: 1;
`;

const CategoryButton = styled(Button)`
  margin-right: .1em;
  margin-bottom: .1em;
`;

class GiftFilter extends Component {
  static propTypes = {
    onFilter: PropTypes.func.isRequired,
    categories: PropTypes.array,
    params: PropTypes.object,
  };

  static defaultProps = {
    categories: [],
    params: {},
  };

  constructor(props) {
    super(props);
    this.state = { categories: [], search: '', available: false };
    this.filter = debounce(this.props.onFilter, 250);
  }

  handleSearchChange(search) {
    this.setState({ search }, () => this.filter(this.state));
  }

  handleCategoryClick(id) {
    const index = this.state.categories.indexOf(id);
    if (index >= 0) {
      const categories = this.state.categories;
      categories.splice(index, 1);
      this.setState(
        { categories },
        () => this.filter(this.state));
    } else {
      this.setState(
        { categories: [...this.state.categories, id] },
        () => this.filter(this.state));
    }
  }

  handleAvailableClick() {
    const available = !this.state.available;
    this.setState({ available }, () => this.filter(this.state));
  }

  isActive(id) {
    return this.state.categories.indexOf(id) >= 0;
  }

  render() {
    return (<Wrapper {...this.props}>
      <Search value={this.state.search} onChange={(e) => this.handleSearchChange(e.target.value)} placeholder="Cerca..." />
      <Categories>
        {this.props.categories.map(({ id, name }) => (
          <CategoryButton key={id} transparent={this.state.categories.indexOf(id) < 0} onClick={() => this.handleCategoryClick(id)}>
            {name}
          </CategoryButton>
        ))}
      </Categories>
      <Button transparent={!this.state.available} onClick={() => this.handleAvailableClick()}>
        Solo disponibili
      </Button>
    </Wrapper>);
  }
}

export default GiftFilter;
