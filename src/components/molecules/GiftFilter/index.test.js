import React from 'react';
import { shallow } from 'enzyme';
import GiftFilter from '.';

const wrap = (props) => shallow(<GiftFilter {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ foo: 'bar' });
  expect(wrapper.find({ foo: 'bar' })).toHaveLength(1);
});
