import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftFilter } from 'components';

const props = {
  onFilter: () => {},
  categories: [
    { id: 1, name: 'Salotto' },
    { id: 2, name: 'Elettrodomestici' },
    { id: 4, name: 'Cucina' },
  ],
};
storiesOf('GiftFilter', module)
  .add('default', () => (
    <GiftFilter {...props} />
  ));
