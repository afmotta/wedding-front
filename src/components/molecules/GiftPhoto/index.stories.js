import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftPhoto } from 'components';

const gift = {
  id: 1,
  name: 'Gift 1',
  description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.',
};

storiesOf('GiftPhoto', module)
  .add('default', () => (
    <GiftPhoto gift={gift} />
  ))
  .add('loading', () => (
    <GiftPhoto gift={{}} loading />
  ));
