import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { media } from 'services/style/utils';

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  padding: 2em;
  display: block;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  ${media.min.md`flex-direction: row;`}
`;

const Photo = styled.img`
  width: 100%;
  ${media.min.md`width: 50%;`}
`;

const Info = styled.div`
  width: 100%;
  ${media.min.md`width: 50%;`}
`;

const GiftPhoto = ({ children, gift, loading, ...props }) => {
  return (
    <Wrapper {...props}>
      <Photo src={gift.photoDetailCrop} />
      <Info>
        {children}
      </Info>
    </Wrapper>
  );
};

GiftPhoto.propTypes = {
  children: PropTypes.node,
  gift: PropTypes.object.isRequired,
  loading: PropTypes.bool,
};

export default GiftPhoto;
