import React from 'react';
import { storiesOf } from '@storybook/react';
import { Tagline } from 'components';

storiesOf('Tagline', module)
  .add('default', () => (
    <Tagline>Hello</Tagline>
  ))
  .add('reverse', () => (
    <Tagline reverse>Hello</Tagline>
  ));
