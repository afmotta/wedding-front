import React from 'react';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Heading } from 'components';

const Wrapper = styled.div`
  background-color: ${palette('white', 2)};
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  padding: 25px 0;
`;

const Main = styled(Heading)`
  font-family: ${font('serif')};
  color: ${palette('primary', 0)};
  margin: 0;
  line-size: 1;
`;

const Sub = styled(Heading)`
  margin: 0;
  font-size: 1.2rem;
`;

const Tagline = (props) => {
  return (
    <Wrapper {...props}>
      <Main level={2}>Benvenuti nel sito dedicato al nostro matrimonio.</Main>
      <Sub level={6}>Siamo felici di condividere con voi i più bei momenti di questo giorno.</Sub>
    </Wrapper>
  );
};

Tagline.propTypes = {
};

export default Tagline;
