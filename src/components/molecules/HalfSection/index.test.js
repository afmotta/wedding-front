import React from 'react';
import { shallow } from 'enzyme';
import HalfSection from '.';

const data = {
  photo: '/giuly.jpg',
};
const wrap = (props = data) => shallow(<HalfSection {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo', ...data });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(3);
});
