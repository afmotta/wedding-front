import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { font, palette } from 'styled-theme';
import { media } from 'services/style/utils';

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: block;
  height: 380px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  ${media.min.md`flex-direction: row;`}
`;

const responsive = css`
  height: 100%;
  width: 100%;
  ${media.min.md`width: 50%;`}
`;

const Photo = styled.div`
  background-image: url(${props => props.photo});
  background-size: cover;
  background-position: ${props => props.photoAlign};
  background-repeat: no-repeat;
  ${responsive}
  order: 1;
  ${media.min.md`order: ${props => props.inverted ? 2 : 1};`}
`;

const Text = styled.div`
  ${responsive}
  text-align: center;
  order: 2;
  ${media.min.md`order: ${props => props.inverted ? 1 : 2};`}
`;

const HalfSection = ({ children, ...props }) => {
  return (
    <Wrapper {...props}>
      <Text {...props}>
        {children}
      </Text>
      <Photo {...props} />
    </Wrapper>
  );
};

HalfSection.propTypes = {
  inverted: PropTypes.bool,
  children: PropTypes.node,
};

export default HalfSection;
