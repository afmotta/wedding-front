import React from 'react';
import { storiesOf } from '@storybook/react';
import { HalfSection } from 'components';

const props = {
  photo: '/giuly.jpg',
};
storiesOf('HalfSection', module)
  .add('default', () => <HalfSection {...props} />);
