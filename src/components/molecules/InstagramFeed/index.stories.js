import React from 'react';
import { storiesOf } from '@storybook/react';
import { InstagramFeed } from 'components';

const photos = [
  '/giuly1.jpg',
  '/giuly2.jpg',
  '/giuly3.jpg',
  '/giuly4.jpg',
  '/giuly5.jpg',
  '/giuly6.jpg',
  '/giuly7.jpg',
  '/giuly8.jpg',
  '/giuly9.jpg',
];

storiesOf('InstagramFeed', module)
  .add('default', () => (
    <InstagramFeed photos={photos}>Hello</InstagramFeed>
  ));
