import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Link, Heading, Button } from 'components';

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  background-color: ${palette('grayscale', 4)};
  text-align: center;
`;

const Head = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding: 1em 0;
  width: 100%;
`;

const Hashtag = styled(Heading)`
  font-size: 2.5rem;
`;

const Grid = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-top: 15px;
  padding: 1em 1em 2em;
  box-sizing: border-box;
  width: 100%;
`;

const Photo = styled.div`
  width: 25vw;
  height: 25vw;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-image: url(${props => props.photo});
`;

const StyledButton = styled(Button)`
  margin-bottom: 2em;
`;

const InstagramFeed = ({ list, request, hasNext }) => {
  return (
    <Wrapper>
      <Head>
        <Heading level={6}>Instagram Feed</Heading>
        <Hashtag level={2}>#giulyalbitop</Hashtag>
      </Head>
      <Grid>
        {list.map(({ image, link }) => (
          <Link key={image} to={link} target="blank">
            <Photo photo={image} />
          </Link>))}
      </Grid>
      {hasNext && <StyledButton onClick={request}>altre</StyledButton>}
    </Wrapper>
  );
};

InstagramFeed.propTypes = {
  list: PropTypes.array,
  request: PropTypes.func,
  hasNext: PropTypes.bool,
};

InstagramFeed.defaultProps = {
  list: [],
  hasNext: false,
};

export default InstagramFeed;
