import React from 'react';
import { storiesOf } from '@storybook/react';
import { TitleSection } from 'components';

storiesOf('TitleSection', module)
  .add('default', () => (
    <TitleSection>Hello</TitleSection>
  ))
  .add('reverse', () => (
    <TitleSection reverse>Hello</TitleSection>
  ));
