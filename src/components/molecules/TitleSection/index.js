import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Heading } from 'components';

const Wrapper = styled.div`
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  margin: 20px 0;
`;

const Title = styled(Heading)`
  font-family: ${font('serif')};
  color: ${palette('primary', 0)};
  margin-bottom: 0;
`;

const Divide = styled.hr`
  width: 40%;
  margin-top: 0;
  margin-bottom: 20px;
  color: ${palette('primary', 0)};
`;

const TitleSection = ({ title, ...props }) => {
  return (
    <Wrapper {...props}>
      <Title level={1} {...props}>
        {title}
      </Title>
      <Divide />
    </Wrapper>
  );
};

TitleSection.propTypes = {
  title: PropTypes.string.isRequired,
  reverse: PropTypes.bool,
  children: PropTypes.node,
};

export default TitleSection;
