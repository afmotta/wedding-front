import React from 'react';
import { storiesOf } from '@storybook/react';
import { Infoline } from 'components';

storiesOf('Infoline', module)
  .add('default', () => (
    <Infoline>Hello</Infoline>
  ))
  .add('reverse', () => (
    <Infoline reverse>Hello</Infoline>
  ));
