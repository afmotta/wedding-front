import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Heading } from 'components';

const Wrapper = styled.div`
  background-color: ${palette('white', 2)};
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  padding: 20px 0;
  margin: 20px 0;
`;

const Sub = styled(Heading)`
  margin: 0;
  font-size: 1.2rem;
  & > a {
    text-decoration: underline;
    cursor: pointer;
  }
`;

const Infoline = (props) => {
  return (
    <Wrapper {...props}>
      <Sub level={6}>
        Per chi lo desiderasse, c'è la possibilità di pernottare in alberghi convenzionati, nelle immediate vicinanze della tenuta.
      </Sub>
      <Sub level={6}>
        <a target="blank" onClick={props.onClick} >
          Contattaci
        </a>
      </Sub>
    </Wrapper>
  );
};

Infoline.propTypes = {
  onClick: PropTypes.func,
};

export default Infoline;
