import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { media } from 'services/style/utils';
import { palette } from 'styled-theme';
import RouterLink from 'react-router-dom/NavLink';
import truncate from 'lodash/truncate';
import { Heading, Paragraph, Badge } from 'components';

const Card = styled(RouterLink)`
  position: relative;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-content: stretch;
  margin-bottom: 20px;
  background: ${palette('grayscale', 6)};
  color: #363636;
  text-decoration: none;
  height: 19em;
  width: 80%;
  border: .1px solid ${palette('grayscale', 5)};
  ${media.min.sm`width: 48%; margin-right: 1%;`}
  ${media.min.lg`width: 32%; margin-right: 1%;`}
  ${media.min.xxl`width: 23.5%; margin-right: 1%;`}
`;

const CardHeader = styled.div`
  position: relative;
  height: 175px;
  overflow: hidden;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-color: rgba(white,.15);
  background-blend-mode: overlay;
  background-image: url(${props => props.photo});
`;

const CardTitle = styled(Heading)`
  background: rgba(0, 0, 0, .5);
  padding: 3.5% 2.5% 2.5% 2.5%;
  color: white;
  text-transform: uppercase;
  position: absolute;
  bottom: 0;
  width: 100%;
  margin: 0;
`;

const CardDescription = styled(Paragraph)`
  margin-top: 5px;
  padding: 0 15px;
  overflow: hidden;
  text-overflow: ellipsis;
  height: 60px;
`;

const CardBadges = styled.div`
  padding-bottom: 10px;
  padding-right: 5px;
  align-self: flex-end;
`;

const GiftItem = ({ id, name, photoListCrop, description, availableParts }) => {
  return (
    <Card to={`/gifts/${id}`}>
      <CardHeader photo={photoListCrop}>
        <CardTitle level={6}>{name}</CardTitle>
      </CardHeader>
      <CardDescription>
        {truncate(description, { length: 80, separator: ' ' }) }
      </CardDescription>
      <CardBadges>
        {availableParts ? (
          <Badge>Disponibile</Badge>
        ) : (
          <Badge palette="danger">Terminato</Badge>
        )}
      </CardBadges>
    </Card>
  );
};

GiftItem.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  price: PropTypes.number,
  description: PropTypes.string,
  photoListCrop: PropTypes.string,
  availableParts: PropTypes.number,
};

export default GiftItem;
