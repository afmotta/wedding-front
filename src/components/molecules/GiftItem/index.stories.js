import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftItem } from 'components';
import StoryRouter from 'storybook-router';

storiesOf('GiftItem', module)
  .addDecorator(StoryRouter())
  .addWithInfo(
    'default',
    () => (<GiftItem id={1} name="Gift n.1" />),
    { propTables: [GiftItem] }
);
