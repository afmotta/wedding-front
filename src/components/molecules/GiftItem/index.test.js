import React from 'react';
import { mount, shallow } from 'enzyme';
import GiftItem from '.';

const wrap = (props = {}) => shallow(<GiftItem id={1} name="test gift" {...props} />).dive();

it('mounts with different combination of props', () => {
  const wrapMounted = (props = {}) => mount(<GiftItem id={2} name="a" body="b" {...props} />);
  wrapMounted();
});

it('renders name', () => {
  const wrapper = wrap();
  expect(wrapper.contains('test gift')).toBe(true);
});
