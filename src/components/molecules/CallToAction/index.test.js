import React from 'react';
import { shallow } from 'enzyme';
import CallToAction from '.';

const wrap = (props = {}) => shallow(<CallToAction {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo' });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(1);
});
