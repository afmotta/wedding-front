import React from 'react';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Button, Heading, HorizontalRule } from 'components';

const Wrapper = styled.div`
  background-color: ${palette('primary', 0)};
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  padding: 25px 0;
`;

const Title = styled(Heading)`
  color: ${palette('white', 0)};
  font-family: ${font('serif')};
  margin-bottom: 0;
`;

const Hr = styled(HorizontalRule)`
  width: 40%;
  margin-top: 0;
  margin-bottom: 20px;
`;

const Text = styled(Heading)`
  color: ${palette('white', 0)};
  padding: 0 80px;
  text-align: center;
`;

const CtAButton = styled(Button)`
  margin: 20px;
  font-size: 1.3rem;
`;

const CallToAction = (props) => {
  return (
    <Wrapper {...props}>
      <Title>Lista nozze</Title>
      <Hr palette={'white'} />
      <Text level={6}>
        Chi volesse contribuire alla nascita
        della nostra famiglia o al nostro viaggio in Alaska,
        può consultare la nostra lista nozze.
      </Text>
      <CtAButton to={'gifts'} transparent palette={'white'}>
        Fai un regalo agli sposi
      </CtAButton>
    </Wrapper>
  );
};

export default CallToAction;
