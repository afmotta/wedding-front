import React from 'react';
import { storiesOf } from '@storybook/react';
import { CallToAction } from 'components';

storiesOf('CallToAction', module)
  .add('default', () => (
    <CallToAction>Hello</CallToAction>
  ))
  .add('reverse', () => (
    <CallToAction reverse>Hello</CallToAction>
  ));
