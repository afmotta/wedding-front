import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, PartsSelect } from 'components';

const Wrapper = styled.div``;

const ButtonWrapper = styled.div`
  vertical-align: middle;
  display: inline-flex;
`;

class GiftAcquire extends Component {
  constructor(props) {
    super(props);
    this.state = { parts: 1 };
  }

  setParts(parts) {
    this.setState({ parts });
  }

  render() {
    const { initAcquire, gift } = this.props;

    if (!gift.availableParts) {
      return <div />;
    }

    return (
      <Wrapper>
        <PartsSelect name="parts" total={gift.availableParts} onChange={({ target: { value } }) => this.setParts(Number(value))} />
        <ButtonWrapper>
          <Button onClick={() => initAcquire(gift, this.state.parts)}>
            Regala agli sposi
          </Button>
        </ButtonWrapper>
      </Wrapper>
    );
  }
}

GiftAcquire.propTypes = {
  initAcquire: PropTypes.func.isRequired,
  gift: PropTypes.object,
};

export default GiftAcquire;
