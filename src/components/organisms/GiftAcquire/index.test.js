import React from 'react';
import { shallow } from 'enzyme';
import GiftAcquire from '.';

const props = {
  gift: {
    availableParts: 3,
  },
  initAcquire() {},
};

const wrap = (props = {}) => shallow(<GiftAcquire {...props} />).dive();

it('renders PartsSelect', () => {
  const wrapper = wrap(props);
  expect(wrapper.find('PartsSelect')).toHaveLength(1);
});
