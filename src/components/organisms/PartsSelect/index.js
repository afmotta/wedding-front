import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import range from 'lodash/range';

import { Input } from 'components';

const Wrapper = styled.div`
  display: inline-flex;
  margin-right: 20px;
`;

const PartsSelect = (props) => {
  const { total, selected } = props;
  return (
    <Wrapper>
      <Input type="select" defaultValue={selected} {...props}>
        {range(1, total + 1).map(i => <option key={i}>{i}</option>)}
      </Input>
    </Wrapper>
  );
};

PartsSelect.propTypes = {
  total: PropTypes.number.isRequired,
  selected: PropTypes.number,
  onChange: PropTypes.func,
};

export default PartsSelect;
