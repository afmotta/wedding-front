import React from 'react';
import { shallow } from 'enzyme';
import PartsSelect from '.';

const onChange = jest.fn();
const wrap = (props = {}) => shallow(<PartsSelect {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ total: 2, onChange });
  expect(wrapper.find('option')).toHaveLength(2);
});
