import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftDetail } from 'components';

const gift = {
  id: 1,
  name: 'Gift 1',
  description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.',
};

storiesOf('GiftDetail', module)
  .add('default', () => (
    <GiftDetail gift={gift} />
  ))
  .add('loading', () => (
    <GiftDetail gift={{}} loading />
  ));
