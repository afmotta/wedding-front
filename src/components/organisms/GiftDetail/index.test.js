import React from 'react';
import { shallow } from 'enzyme';
import GiftDetail from '.';

const gift = { id: 1, title: 'title 1', body: 'body 1' };

const wrap = (props = {}) => shallow(<GiftDetail gift={gift} {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo' });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(1);
});

it('renders loading when passed in', () => {
  const wrapper = wrap({ loading: true });
  expect(wrapper.contains('Loading')).toBe(true);
});
