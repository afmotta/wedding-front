import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { GiftPhoto, GiftInfo, Spinner } from 'components';
import { GiftAcquire } from 'containers';

const Detail = styled.div`
  padding: 0 2em;
  & > * {
    margin-bottom: 2em;
    text-align: center;
  }
`;

const GiftDetail = ({ gift, loading, ...props }) => {
  if (loading) {
    return <Spinner />;
  }
  return (
    <GiftPhoto gift={gift} {...props}>
      <Detail>
        <GiftInfo key={gift.id} {...gift} />
        <GiftAcquire gift={gift} />
      </Detail>
    </GiftPhoto>
  );
};

GiftDetail.propTypes = {
  gift: PropTypes.object.isRequired,
  loading: PropTypes.bool,
};

export default GiftDetail;
