import React from 'react';
import PropTypes from 'prop-types';
import { Heading, Paragraph, HalfSection } from 'components';

const Spouse = (props) => {
  return (
    <HalfSection {...props}>
      <Heading level={3}>{props.fullname}</Heading>
      <Heading level={5}>aka</Heading>
      <Heading level={2}>{props.nickname}</Heading>
      <Paragraph>{props.bio}</Paragraph>
    </HalfSection>
  );
};

Spouse.propTypes = {
  fullname: PropTypes.string.isRequired,
  nickname: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  inverted: PropTypes.bool,
};

export default Spouse;
