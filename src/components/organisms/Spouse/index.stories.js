import React from 'react';
import { storiesOf } from '@storybook/react';
import { Spouse } from 'components';

const props = {
  fullname: 'Spouse full name',
  nickname: 'Spouse nickname',
  bio: 'Lorem ipsum',
  photo: '/giuly.jpg',
};
storiesOf('Spouse', module)
  .add('default', () => <Spouse {...props} />);
