import React from 'react';
import { shallow } from 'enzyme';
import Albi from '.';

const data = {
  fullname: 'Spouse full name',
  nickname: 'Spouse nickname',
  bio: 'Lorem ipsum',
  photo: '/giuly.jpg',
};
const wrap = (props = data) => shallow(<Albi {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo', ...data });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(3);
});
