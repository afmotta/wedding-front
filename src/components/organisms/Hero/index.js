import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { palette } from 'styled-theme';

import { Block, Heading, Button } from 'components';

const Wrapper = styled(Block)`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: flex-end;
  align-content: stretch;
  align-items: center;
  height: calc(100vh - 3.75rem);
  padding: 2rem 6rem;
  box-sizing: border-box;
  text-align: center;
  background-image: url('/bg-hero.jpg');
  background-size: cover;
  background-position: center;
  @media screen and (max-width: 640px) {
    padding: 1rem;
  }
`;

const Title = styled(Heading)`
  color: ${palette('white', 0)};
  max-width: 800px;
  font-weight: 300;
  font-size: 3.5rem;
  letter-spacing: 0.07em;
  flex: 2;
  text-shadow: 0 0 2px ${palette('grayscale', 0)};
  @media screen and (max-width: 640px) {
    font-size: 2.5 rem;
  }
`;

const And = styled.span`
  font-size: 2.5rem;
`;

const Tagline = styled(Heading)`
  color: ${palette('white', 0)};
  text-shadow: 0 0 2px ${palette('grayscale', 0)};
`;

const Flex = styled.div`
  margin-bottom: 1.6em;
`;

const RSVPButton = styled(Button)`
  color: ${palette('grayscale', 0)};
`;

const Hero = (props) => {
  return (
    <Wrapper reverse>
      <Flex>
        <Title level={1}>Giulietta <And>e</And> Alberto</Title>
        <Tagline level={2}>3 Settembre 2017</Tagline>
      </Flex>
      <RSVPButton href="" height={50} onClick={props.onClick} transparent reverse>
        RSVP
      </RSVPButton>
    </Wrapper>
  );
};

Hero.propTypes = {
  onClick: PropTypes.func,
};

export default Hero;
