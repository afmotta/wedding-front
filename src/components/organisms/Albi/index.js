import React from 'react';
import { Spouse } from 'components';

const data = {
  fullname: 'Alberto Francesco Motta',
  nickname: 'Albi',
  bio: 'Lorem ipsum',
  photo: '/albi.jpg',
  photoAlign: 'left',
};

const Albi = (props) => <Spouse {...{ ...props, ...data }} />;

export default Albi;
