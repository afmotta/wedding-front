import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-router';

import { AcquireForm } from 'components';

storiesOf('AcquireForm', module)
  .addDecorator(StoryRouter())
  .add('default', () => (
    <AcquireForm />
  ));
