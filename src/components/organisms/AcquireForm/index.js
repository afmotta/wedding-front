import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import styled from 'styled-components';
import range from 'lodash/range';

import { ReduxField, Heading, Button } from 'components';

const Form = styled.form`
  box-sizing: border-box;
  padding: 1rem;
`;

class AcquireForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool,
    gift: PropTypes.object,
    parts: PropTypes.number,
    initialize: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = { selectedParts: props.parts || 1 };
  }

  componentDidMount() {
    const { initialize, parts, gift: { id } } = this.props;
    initialize({ parts, id });
  }

  updateParts(e) {
    this.setState({ selectedParts: e.target.value });
  }

  render() {
    const { gift, handleSubmit, submitting } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
        <Heading level={2}>Invia il tuo regalo</Heading>
        <Heading level={4}>{gift.name}</Heading>
        <Field name="gift_id" type="hidden" component={ReduxField} />
        <Field
          name="parts"
          label={`Parti (€ ${gift.partPrice * this.state.selectedParts})`}
          type="select" onChange={(e) => this.updateParts(e)}
          component={ReduxField}
        >
          {range(1, gift.availableParts + 1).map(i => <option key={i}>{i}</option>)}
        </Field>
        <Field name="message" label="Il tuo messaggio per gli sposi" type="textarea" component={ReduxField} />
        <Field name="giver" label="Firmato" component={ReduxField} />
        <Button type="submit" disabled={submitting}>Invia</Button>
      </Form>
    );
  }
}

export default AcquireForm;
