import React from 'react';
import { Spouse } from 'components';

const data = {
  fullname: 'Giulietta Paraboni',
  nickname: 'Giuly',
  bio: 'Lorem ipsum',
  photo: '/giuly.jpg',
};

const Giuly = (props) => <Spouse {...{ ...props, ...data }} />;

export default Giuly;
