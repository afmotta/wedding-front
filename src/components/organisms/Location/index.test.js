import React from 'react';
import { shallow } from 'enzyme';
import Location from '.';

const wrap = (props = {}) => shallow(<Location {...props} />);

it('renders address when passed in', () => {
  const wrapper = wrap({ address: 'test' });
  expect(wrapper.contains('test')).toBe(true);
});

it('renders props when passed in', () => {
  const wrapper = wrap({ id: 'foo' });
  expect(wrapper.find({ id: 'foo' })).toHaveLength(1);
});
