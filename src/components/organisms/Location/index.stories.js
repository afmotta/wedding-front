import React from 'react';
import { storiesOf } from '@storybook/react';
import { Location } from 'components';

storiesOf('Location', module)
  .add('default', () => (
    <Location>Hello</Location>
  ))
  .add('reverse', () => (
    <Location reverse>Hello</Location>
  ));
