import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, Heading, HalfSection } from 'components';

const Flex = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled(Heading)`
  font-weight: 300;
  margin-top: 0;
  margin-bottom: 0.6rem;
`;

const Subtitle = styled(Title)`
  font-size: 1.3rem;
`;

const Map = styled(Button)`
  margin-top: 40px;
`;


const Location = (props) => {
  return (
    <HalfSection {...props}>
      <Flex>
        <div>
          {props.time && <Subtitle level={6}>{props.time}</Subtitle>}
          <Title level={2}>{props.name}</Title>
          <Subtitle level={5}>{props.address}</Subtitle>
          <Subtitle level={5}>{props.city}</Subtitle>
        </div>
        <Map href={props.url} target="blank" transparent>Vedi la mappa</Map>
      </Flex>
    </HalfSection>
  );
};

Location.propTypes = {
  name: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  time: PropTypes.string,
  photo: PropTypes.string.isRequired,
  url: PropTypes.string,
};

export default Location;
