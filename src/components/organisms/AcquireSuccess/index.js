import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { font, palette } from 'styled-theme';
import { Heading, Paragraph } from 'components';

const Wrapper = styled.div`
  font-family: ${font('primary')};
  color: ${palette('grayscale', 0)};
`;

const Title = styled(Heading)`
  color: ${palette('primary', 0)};
  font-family: ${font('serif')};
  font-size: 4em;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  padding: 10px 0;
`;
const CostWrapper = styled.div``;

const AcquireSuccess = ({ cost }) => {
  return (
    <Wrapper>
      <Title level={2}>grazie!</Title>
      <CostWrapper>
        Puoi effettuare il pagamento della somma di
         € {cost} tramite bonifico bancario a queste coordinate:
      </CostWrapper>
      <Paragraph>
        <b>Intestatario</b>: Giulietta Paraboni e Alberto Francesco Motta
      </Paragraph>
      <Paragraph>
        <b>IBAN</b>: IT24Y0558401635000000000527
      </Paragraph>
    </Wrapper>
  );
};

AcquireSuccess.propTypes = {
  parts: PropTypes.number,
  cost: PropTypes.number,
};

export default AcquireSuccess;
