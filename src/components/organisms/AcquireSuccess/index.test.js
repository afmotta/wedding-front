import React from 'react';
import { shallow } from 'enzyme';
import AcquireSuccess from '.';

const wrap = (props = {}) => shallow(<AcquireSuccess {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap({ parts: 1, cost: 200 });
});
