import React from 'react';
import { storiesOf } from '@storybook/react';
import { AcquireSuccess } from 'components';

storiesOf('AcquireSuccess', module)
  .add('default', () => (
    <AcquireSuccess parts={1} cost={100} />
  ));
