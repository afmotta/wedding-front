import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-router';

import { ContactForm } from 'components';

storiesOf('ContactForm', module)
  .addDecorator(StoryRouter())
  .add('default', () => (
    <ContactForm />
  ));
