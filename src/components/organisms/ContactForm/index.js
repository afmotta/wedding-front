import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import styled, { css } from 'styled-components';
import { media } from 'services/style/utils';
import { ReduxField, Heading, Button } from 'components';

const Wrapper = styled.div`
  display: block;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-bottom: 20px;
  ${media.min.md`flex-direction: row;`}
`;

const responsive = css`
  height: 100%;
  width: 100%;
  ${media.min.md`width: 50%;`}
`;

const First = styled.div`
  ${responsive};
  order: 1;
`;

const Second = styled.div`
  ${responsive};
  order: 2;
`;

const Text = styled(Heading)`
  padding: 0 1em;
`;

const Form = styled.form`
  box-sizing: border-box;
  width: 100%;
  padding: 0 1em;
`;

const ContactForm = ({ handleSubmit, submitting }) => {
  return (
    <Wrapper>
      <First>
        <Text level={5}>
          Per confermare la vostra presenza e per comunicarci qualsiasi esigenza (alimentare, di alloggio, ecc.),
          scrivete qui a fianco oppure alla email: rsvp@giulyalbi.top
        </Text>
      </First>
      <Second>
        <Form onSubmit={handleSubmit}>
          <Field name="name" label="Il tuo nome" component={ReduxField} />
          <Field name="email" label="La tua email" component={ReduxField} />
          <Field name="message" label="Il tuo messaggio" type="textarea" component={ReduxField} />
          <Button type="submit" disabled={submitting}>Invia</Button>
        </Form>
      </Second>
    </Wrapper>
  );
};

ContactForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
};

export default ContactForm;
