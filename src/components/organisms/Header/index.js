import React from 'react';
import styled from 'styled-components';
import { palette } from 'styled-theme';

import { PrimaryNavigation, Block } from 'components';

const Wrapper = styled(Block)`
  display: flex;
  align-items: center;
  padding: 1rem;

  & > :not(:first-child) {
    margin-left: 1rem;
  }
`;

const StyledHeading = styled.span`
  display: inline-block;
  transform-origin: center;
  font-weight: 300;
  color: ${palette('primary', 0)};
  font-size: 1.25rem;
`;

const StyledPrimaryNavigation = styled(PrimaryNavigation)`
  flex: 1;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-end;
  align-content: stretch;
  align-items: center;
`;

const Header = (props) => {
  return (
    <Wrapper opaque {...props}>
      <StyledHeading level={3} height={100}>Giuly & Albi</StyledHeading>
      <StyledPrimaryNavigation />
    </Wrapper>
  );
};

export default Header;
