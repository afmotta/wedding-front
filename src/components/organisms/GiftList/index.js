import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { media } from 'services/style/utils';
import { GiftFilter, GiftItem, Spinner } from 'components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${media.min.md`
    flex-direction: row;
  `}
`;

const Sidebar = styled.div`
  flex: 0;
  align-self: stretch;
  padding: 1em;
  display: none;
  ${media.min.md`
    flex: 0 0 14em;
    display: flex;
  `}
`;

const Filter = styled(GiftFilter)`
  height: 80vh;
`;

const Grid = styled.div`
  flex: 1;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-top: 15px;
  padding: 0 3%;
  box-sizing: border-box;
  width: 100%;
  ${media.min.sm`justify-content: space-between;`}
  ${media.min.xl`
    &:after {
      content: "";
      flex: auto;
    }
  `}
`;

const GiftList = ({ list, loading, request, categories, ...props }) => {
  return (
    <Wrapper {...props}>
      <Sidebar><Filter onFilter={request} categories={categories} /></Sidebar>
      {loading ?
        <Spinner /> :
        <Grid>
          {list.map(gift => <GiftItem key={gift.id} {...gift} />)}
        </Grid>
      }
    </Wrapper>
  );
};

GiftList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  request: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  categories: PropTypes.array,
};

export default GiftList;
