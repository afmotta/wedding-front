import React from 'react';
import { storiesOf } from '@storybook/react';
import { GiftList } from 'components';
import StoryRouter from 'storybook-router';

const list = [
  { id: 0, name: 'Post 1', description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.' },
  { id: 1, name: 'Post 1', description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.' },
  { id: 2, name: 'Post 1', description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.' },
  { id: 3, name: 'Post 1', description: 'Voluptate ullamco anim exercitation deserunt cillum ullamco.' },
];

storiesOf('GiftList', module)
  .addDecorator(StoryRouter())
  .add('default', () => (
    <GiftList list={list} />
  ))
  .add('loading', () => (
    <GiftList list={[]} loading />
  ));
