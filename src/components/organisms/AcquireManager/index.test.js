import React from 'react';
import { shallow } from 'enzyme';
import AcquireManager from '.';

const data = { reset: () => {}, step: 'form', gift: {}, parts: 1 };
const wrap = (props = data) => shallow(<AcquireManager {...props} />);

it('renders props when passed in', () => {
  const wrapper = wrap(data);
  expect(wrapper.find(data)).toHaveLength(1);
});
