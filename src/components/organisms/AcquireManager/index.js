import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { AcquireSuccess, GiftPhoto } from 'components';
import { AcquireForm } from 'containers';

class AcquireManager extends Component {
  static propTypes = {
    cost: PropTypes.number,
    gift: PropTypes.object,
    parts: PropTypes.number,
    reset: PropTypes.func.isRequired,
    step: PropTypes.string,
  };

  componentWillUnmount() {
    this.props.reset();
  }

  getComponent() {
    switch (this.props.step) {
      case 'form': return <AcquireForm />;
      case 'success': return <AcquireSuccess {...this.props} />;
      default: return <Redirect to="/" push />;
    }
  }

  render() {
    if (!this.props.gift || !this.props.parts) {
      return <Redirect to="/" push />;
    }
    return (
      <GiftPhoto {...this.props}>
        {this.getComponent()}
      </GiftPhoto>
    );
  }
}

export default AcquireManager;
